package com.buyaroffTest.InteractiveMaps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.buyaroffTest.InteractiveMaps.adapters.PointAdapter;
import com.buyaroffTest.InteractiveMaps.database.DatabaseManager;
import com.buyaroffTest.InteractiveMaps.model.Point;

import java.util.List;


public class ListPointsActivity extends Activity {

    ListView mainList;
    ImageView imageBack;
    List<Point> list;
    PointAdapter adapter;
    AlertDialog.Builder deleteDialog;
    int currentPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.points_list);
        createDeleteDialog();
        mainList = (ListView) findViewById(R.id.pointsList);

        list = DatabaseManager.getInstance().getAllPoints();
        if (list != null) {
            adapter = new PointAdapter(this, list);
            mainList.setAdapter(adapter);
            mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Point point = list.get(position);
                    if (point != null) {
                        int pointId = point.getId();
                        Intent intent = new Intent();
                        intent.putExtra("pointId", pointId);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });
        }  else {
            Toast.makeText(getApplicationContext(),
                    "You have no markers yet.\n Please, add at least one.", Toast.LENGTH_SHORT).show();
        }


        mainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                currentPos = position;
                deleteDialog.show();

                return false;
            }
        });

        imageBack = (ImageView) findViewById(R.id.imageBack);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void createDeleteDialog() {
        deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle("Delete point?");  // заголовок
        deleteDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                if (currentPos > -1) {
                    Point point = list.get(currentPos);
                    if (point != null) {
                        DatabaseManager.getInstance().deletePoint(point);
                        list.remove(point);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
        deleteDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });
        deleteDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
    }
}
