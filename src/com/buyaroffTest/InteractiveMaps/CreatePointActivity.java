package com.buyaroffTest.InteractiveMaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.buyaroffTest.InteractiveMaps.database.DatabaseManager;
import com.buyaroffTest.InteractiveMaps.model.Point;


public class CreatePointActivity extends Activity {

    private double currentLat;
    private double currentLon;
    private String currentTitle = "";
    private String currentDesc = "";
    private int pointId;


    Button buttonCncl;
    Button buttonClr;
    Button buttonOK;

    EditText titleEd;
    EditText descEd;

    TextView titleText;
    TextView latitudeTV;
    TextView longitudeTV;

    ImageView imageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_point);

        Intent intent = getIntent();
        pointId = intent.getIntExtra("pointId", 0);
        currentLat = intent.getDoubleExtra("currentLat", 0d);
        currentLon = intent.getDoubleExtra("currentLon", 0d);

        buttonCncl = (Button) findViewById(R.id.buttonCncl);
        buttonClr = (Button) findViewById(R.id.buttonClr);
        buttonOK = (Button) findViewById(R.id.buttonOK);

        titleEd = (EditText) findViewById(R.id.titleEd);
        descEd = (EditText) findViewById(R.id.descEd);

        latitudeTV = (TextView) findViewById(R.id.latitudeTV);
        longitudeTV = (TextView) findViewById(R.id.longitudeTV);
        titleText = (TextView) findViewById(R.id.titleText);

        imageBack = (ImageView) findViewById(R.id.imageBack);
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonCncl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonClr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContent();
                saveMarker();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        if (pointId != 0) {
            titleText.setText(getResources().getString(R.string.edit_point));
            Point p = DatabaseManager.getInstance().getPoint(pointId);
            currentTitle = p.getTitle();
            currentDesc = p.getDescription();
        }
        setContent();
    }

    private void getContent() {
        currentTitle = titleEd.getText().toString();
        currentDesc = descEd.getText().toString();
    }

    private void saveMarker() {
        DatabaseManager manager = DatabaseManager.getInstance();
        Point point;
        if (pointId != 0) {
            point = manager.getPoint(pointId);
        } else {
            point = new Point();
        }
        if (point != null) {
            point.setTitle(currentTitle);
            point.setDescription(currentDesc);
            point.setLatitude(currentLat);
            point.setLongitude(currentLon);
            if (pointId != 0) {
                manager.updatePoint(point);
            } else {
                manager.newPoint(point);
            }
        }
    }

    private void clear() {
        currentTitle = "";
        currentDesc = "";
        setContent();
    }

    private void setContent() {
        titleEd.setText(currentTitle);
        descEd.setText(currentDesc);
        latitudeTV.setText(String.valueOf(currentLat));
        longitudeTV.setText(String.valueOf(currentLon));
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getContent();
        outState.putString("currentTitle", currentTitle);
        outState.putString("currentDesc", currentDesc);
        outState.putInt("pointId", pointId);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentTitle = savedInstanceState.getString("currentTitle");
        currentDesc = savedInstanceState.getString("currentDesc");
        pointId = savedInstanceState.getInt("pointId");
        setContent();
    }
}
