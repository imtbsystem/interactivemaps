package com.buyaroffTest.InteractiveMaps.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.buyaroffTest.InteractiveMaps.R;
import com.buyaroffTest.InteractiveMaps.database.DatabaseManager;
import com.buyaroffTest.InteractiveMaps.model.Point;

import java.util.List;


public class PointAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    List<Point> points;
    private TextView pointTitle;
    private DatabaseManager manager;

    public PointAdapter(Context context, List<Point> points) {
        this.points = points;
        ctx = context;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        manager = DatabaseManager.getInstance();
    }

    @Override
    public int getCount() {
        return points.size();
    }

    @Override
    public Object getItem(int position) {
        return points.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_list, parent, false);
        }
        final Point point = getKey(position);
        String title = point.getTitle();
        if (title != null && title.equals("")) {
            title = ctx.getResources().getString(R.string.untitled_point);
        }
        pointTitle = (TextView) view.findViewById(R.id.point_one);
        pointTitle.setText(title);

        return view;
    }

    Point getKey(int position) {
        Point point = (Point) getItem(position);
        return point;
    }

}
