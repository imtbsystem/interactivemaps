package com.buyaroffTest.InteractiveMaps.application;

import android.view.MenuItem;

public class AppInstance {

    static private AppInstance instance;
    private boolean location = false;
    private MenuItem buttonDone;
    private MenuItem buttonClear;
    private boolean isCurrentMarker;
    private int currentId;
    private double currentLat;
    private double currentLon;

    static public AppInstance getInstance() {
        if (null == instance) {
            instance = new AppInstance();
        }
        return instance;
    }


    public boolean isLocation() {
        return location;
    }

    public void setLocation(boolean location) {
        this.location = location;
    }

    public MenuItem getButtonDone() {
        return buttonDone;
    }

    public void setButtonDone(MenuItem buttonDone) {
        this.buttonDone = buttonDone;
    }

    public MenuItem getButtonClear() {
        return buttonClear;
    }

    public void setButtonClear(MenuItem buttonClear) {
        this.buttonClear = buttonClear;
    }

    public boolean isCurrentMarker() {
        return isCurrentMarker;
    }

    public void setCurrentMarker(boolean currentMarker) {
        isCurrentMarker = currentMarker;
    }

    public double getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(double currentLat) {
        this.currentLat = currentLat;
    }

    public double getCurrentLon() {
        return currentLon;
    }

    public void setCurrentLon(double currentLon) {
        this.currentLon = currentLon;
    }

    public int getCurrentId() {
        return currentId;
    }

    public void setCurrentId(int currentId) {
        this.currentId = currentId;
    }
}
