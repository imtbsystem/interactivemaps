package com.buyaroffTest.InteractiveMaps.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.buyaroffTest.InteractiveMaps.model.Point;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    static private DatabaseManager instance;

    private DatabaseHelper helper;
    SQLiteDatabase DB;
    String TABLE = "points";
    String BASE_NAME = "InteractiveMapsTestDataBase.sqlite";

    static public DatabaseManager getInstance() {
        return instance;
    }

    static public void getInstance(Context ctx) {
        if (null == instance) {
            instance = new DatabaseManager(ctx);
        }
    }

    private DatabaseManager(Context ctx) {
        helper = new DatabaseHelper(ctx, BASE_NAME);
    }

    public void newPoint(Point point) {
        if (point != null) {
            try {
                DB = helper.getWritableDatabase();

                ContentValues insertValues = new ContentValues();
                insertValues.put("title", point.getTitle());
                insertValues.put("description", point.getDescription());
                insertValues.put("latitude", point.getLatitude());
                insertValues.put("longitude", point.getLongitude());
                DB.insert(TABLE, null, insertValues);

                DB.close();
            } catch (Exception e) {
                Log.e("InteractiveMaps", "exception", e);
            }
        }
    }

    public Point getPoint(int id) {
        Point point = null;
        try {
            DB = helper.getReadableDatabase();
            Cursor c = DB.query(TABLE, null, null, null, null, null, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        int id_ = c.getInt(c.getColumnIndex("id"));
                        if (id == id_) {
                            point = selectPoint(c);
                        }
                    } while (c.moveToNext());
                }
                c.close();
            }
            DB.close();
        } catch (Exception e) {
            Log.e("InteractiveMaps", "exception", e);
        }
        return point;
    }

    public List<Point> getAllPoints() {
        List<Point> points = null;
        try {
            DB = helper.getReadableDatabase();
            Cursor c = DB.query(TABLE, null, null, null, null, null, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    points = new ArrayList<>();
                    do {
                        points.add(selectPoint(c));
                    } while (c.moveToNext());
                }
                c.close();
            }
            DB.close();
        } catch (Exception e) {
            Log.e("InteractiveMaps", "exception", e);
        }
        return points;
    }

    public void updatePoint(Point point) {
        if (point != null) {
            try {
                DB = helper.getWritableDatabase();

                ContentValues insertValues = new ContentValues();
                insertValues.put("title", point.getTitle());
                insertValues.put("description", point.getDescription());
                insertValues.put("latitude", point.getLatitude());
                insertValues.put("longitude", point.getLongitude());
                DB.update(TABLE,
                        insertValues,
                        "id = ?",
                        new String[]{String.valueOf(point.getId())});

                DB.close();
            } catch (Exception e) {
                Log.e("InteractiveMaps", "exception", e);
            }
        }
    }

    public void deletePoint(Point point) {
        try {
            DB = helper.getWritableDatabase();
            int i = DB.delete(TABLE, "id = " + point.getId(), null);
            DB.close();
        } catch (Exception e) {
            Log.e("InteractiveMaps", "exception", e);
        }
    }

    private Point selectPoint(Cursor c) {
        Point point = null;
        try {
            point = new Point();
            point.setId(c.getInt(c.getColumnIndex("id")));
            point.setTitle(c.getString(c.getColumnIndex("title")));
            point.setDescription(c.getString(c.getColumnIndex("description")));
            point.setLatitude(c.getDouble(c.getColumnIndex("latitude")));
            point.setLongitude(c.getDouble(c.getColumnIndex("longitude")));
        } catch (Exception e) {
            Log.e("InteractiveMaps", "exception", e);
        }
        return point;
    }


    class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context, String BASE_NAME) {
            super(context, BASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL("CREATE TABLE " + TABLE + " ("
                        + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + "title TEXT,"
                        + "description TEXT,"
                        + "latitude REAL,"
                        + "longitude REAL"
                        + ");");
            } catch (Exception e) {
                Log.e("InteractiveMaps", "exception", e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

}
