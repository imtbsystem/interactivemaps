package com.buyaroffTest.InteractiveMaps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;
import com.buyaroffTest.InteractiveMaps.application.AppInstance;
import com.buyaroffTest.InteractiveMaps.database.DatabaseManager;
import com.buyaroffTest.InteractiveMaps.model.Point;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.util.List;

public class MainActivity extends ActionBarActivity {


    SupportMapFragment mapFragment;
    GoogleMap map;
    Marker marker;

    private LocationManager mlocManager;
    private LocationListener mlocListener;
    private ProgressDialog progressBar;

    final String TAG = "myLogs";
    final int REQUEST_CODE_CREATE = 1;
    final int REQUEST_CODE_EDIT = 2;


    public Handler mapHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    Bundle res = msg.getData();
                    double x = res.getDouble("latitude");
                    double y = res.getDouble("longitude");
                    focusOnPoint(x, y, 10);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        DatabaseManager.getInstance(this);
        if (!AppInstance.getInstance().isLocation()) {
            getGPS();
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Map Pointer");
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#99ffffff")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#99ffffff")));

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map = mapFragment.getMap();
        if (map == null) {
            finish();
            return;
        }
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        AppInstance app = AppInstance.getInstance();
        app.setButtonDone((MenuItem) menu.findItem(R.id.action_done).setVisible(false));
        app.setButtonClear((MenuItem) menu.findItem(R.id.action_clear).setVisible(false));

        refreshMap();

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                break;
            case R.id.action_done:
                done();
                break;
            case R.id.action_clear:
                clearMarkers();
                break;
            case R.id.action_settings:
                showPointsLis();
                break;
        }
        return true;
    }

    private void menuUpdate() {
        AppInstance app = AppInstance.getInstance();
        boolean visible = (app.isCurrentMarker()) ? true : false;
        app.getButtonDone().setVisible(visible);
        app.getButtonClear().setVisible(visible);
        int btnDone = R.drawable.ok;
        int btnClear = R.drawable.clear;
        if (app.getCurrentId() != 0) {
            btnDone = R.drawable.edit;
            btnClear = R.drawable.cancel;
        }
        app.getButtonDone().setIcon(btnDone);
        app.getButtonClear().setIcon(btnClear);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            AppInstance app = AppInstance.getInstance();
            switch (requestCode) {
                case REQUEST_CODE_CREATE:
                    app.setCurrentMarker(false);
                    app.setCurrentId(0);
                    break;
                case REQUEST_CODE_EDIT:
                    int pointId = intent.getIntExtra("pointId", 0);
                    app.setCurrentId(pointId);
                    Point p = DatabaseManager.getInstance().getPoint(pointId);
                    double x = p.getLatitude();
                    double y = p.getLongitude();
                    focusOnPoint(x, y, 13);
                    break;
            }
            refreshMap();
        }
    }


    private void init() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
            }
        });

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
                if (isAllowToAdd()) {
                    addMarker(latLng.latitude, latLng.longitude, "New Marker", null, true);
                }
            }
        });

        UiSettings settings = map.getUiSettings();
        settings.setZoomControlsEnabled(true);
        settings.setCompassEnabled(true);
        settings.setRotateGesturesEnabled(true);
        settings.setTiltGesturesEnabled(true);
        settings.setScrollGesturesEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(false);
        map.setMyLocationEnabled(true);
    }

    private boolean isAllowToAdd() {
        if (AppInstance.getInstance().isCurrentMarker()) {
            Toast.makeText(getApplicationContext(),
                    "Please, confirm current marker at first or cancel it", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void clearMarkers() {
        AppInstance.getInstance().setCurrentMarker(false);
        AppInstance.getInstance().setCurrentId(0);
        refreshMap();
    }

    private void refreshMap() {
        map.clear();
        addMarkersFromBase();
        putOnMapCurrentMarker();
        menuUpdate();
    }

    private void addMarkersFromBase() {
        int currId = AppInstance.getInstance().getCurrentId();
        List<Point> pointList = DatabaseManager.getInstance().getAllPoints();
        if (pointList != null) {
            for (Point point : pointList) {
                boolean color =
                        (AppInstance.getInstance().getCurrentId() == point.getId()) ? true : false;
                addMarker(point.getLatitude(),
                        point.getLongitude(),
                        point.getTitle(),
                        point.getDescription(),
                        color);
            }
        }
    }

    private void addMarker(double lat, double lon, String title, String desc, boolean isNew) {
        float hue = (isNew) ? BitmapDescriptorFactory.HUE_RED : BitmapDescriptorFactory.HUE_GREEN;
        map.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .title(title)
                .snippet(desc)
                .icon(BitmapDescriptorFactory
                        .defaultMarker(hue)));

        if (isNew) {
            AppInstance.getInstance().setCurrentMarker(true);
            AppInstance.getInstance().setCurrentLat(lat);
            AppInstance.getInstance().setCurrentLon(lon);
        }
        menuUpdate();
    }

    private void done() {
        if (AppInstance.getInstance().isCurrentMarker()) {
            Intent intent = new Intent(this, CreatePointActivity.class);
            intent.putExtra("currentLat", AppInstance.getInstance().getCurrentLat());
            intent.putExtra("currentLon", AppInstance.getInstance().getCurrentLon());
            intent.putExtra("pointId", AppInstance.getInstance().getCurrentId());
            startActivityForResult(intent, REQUEST_CODE_CREATE);
        }
    }

    private void showPointsLis() {
        Intent intent = new Intent(this, ListPointsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    private void focusOnPoint(double lat, double lon, int zoom) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lon))
                .zoom(zoom)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.animateCamera(cameraUpdate);
    }

    private void getGPS() {
        progressBar();
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener();
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 100, 0, mlocListener);
    }

    private void stop() {
        if (mlocManager != null) {
            mlocManager.removeUpdates(mlocListener);
        }
        mlocManager = null;
    }

    class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {

            double x = (double) loc.getLatitude();
            double y = (double) loc.getLongitude();

            Message msg = new Message();
            msg.what = 0;
            Bundle data = new Bundle();
            data.putDouble("latitude", x);
            data.putDouble("longitude", y);
            msg.setData(data);
            mapHandler.sendMessage(msg);
            AppInstance.getInstance().setLocation(true);
            try {
                progressBar.dismiss();
            } catch (Exception e) {
                //for cases if USER skip progressBar during process
            }
            stop();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private void progressBar() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Location searching ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();
    }

    private void putOnMapCurrentMarker() {
        String title = "New Marker";
        int pointId = AppInstance.getInstance().getCurrentId();
        if (pointId != 0) {
            Point p = DatabaseManager.getInstance().getPoint(pointId);
            if (p != null) {
                title = p.getTitle();
            }
        }
        if (AppInstance.getInstance().isCurrentMarker() &&
                AppInstance.getInstance().getButtonDone() != null) {
            addMarker(AppInstance.getInstance().getCurrentLat(),
                    AppInstance.getInstance().getCurrentLon(), title, "", true);
        }
    }

}